'''
Created on May 6, 2013

@author: Marko Devcic
'''

from Tkinter import *
from ttk import *
import tkFileDialog
import os.path
import shutil
import tkMessageBox
import thread
import threading
import Queue


kB = 1024.0
MB = kB * kB
GB = MB * MB
title = "fSort"


class RootFrame( Frame ):
	def __init__( self, parent = None ):
		Frame.__init__( self, parent )
		self.master = parent
		self.create_widgets( )
		self.root_directory = ""
		self.queue = Queue.Queue( )
		self.update_items( )

	def insert_tv_item( self, items ):
		self.queue.put( items )

	def update_items( self ):
		try:
			while 1:
				items = self.queue.get_nowait( )
				if items is None:
					self.btn.config(text="Folder ...", state = NORMAL)
					self.master.config(cursor = "")
				elif items[0] == 'root':
					self.tv.set('root', 'Size', items[1])
				else:
					self.tv.insert( "root", END, iid = items[ 0 ], open = 0, text = "",
					                values = (items[ 0 ], items[ 1 ]) )
					
					self.treeview_sort_column( self.tv, "Size", True )

		except Queue.Empty:
			pass
		self.after( 200, self.update_items )


	def create_widgets( self ):
		self.d_columns = ("Path", "Size")
		self.tv = Treeview(self, columns = self.d_columns, displaycolumns = ("Path", "Size"), selectmode = "browse" )
		self.tv.heading( "Size", text = "Size", anchor = W,
		                 command = lambda: self.treeview_sort_column( self.tv, "Size", False ) )
		self.tv.heading( "Path", text = "Path", anchor = W,
		                 command = lambda: self.treeview_sort_column( self.tv, "Path", False ) )
		self.tv.column( "#0", stretch = 0, width = 40 )
		self.tv.column( "Size", width = 80 )
		self.tv.pack(fill='both', expand=1)
		self.tv.bind( "<Button-3>", self.on_right_click )
		self.tv.bind( "<<TreeviewOpen>>", self.on_treeview_open )
		self.menu = Menu( self, tearoff = 0 )
		self.menu.add_command( label = "Delete selected directory tree", command = self.delete_directory )
		self.btn = Button(self, text = "Folder ... ", command = self.on_btn_click)
		self.btn.pack(fill=Y, expand=0, side=BOTTOM, pady=10)

	def on_right_click( self, event ):
		self.menu.post( event.x_root, event.y_root )

	def delete_directory( self ):
		try:
			proceed = tkMessageBox.askyesno( "Warning!",
			                                 "The selected directory and it's subdirectories will be deleted!" )
			d = os.path.join( self.root_directory, (self.tv.set( self.tv.selection( ), "Path" )) )
			if proceed: shutil.rmtree( d )
			if not os.path.exists( d ): self.tv.delete( self.tv.selection( ) )
		except:
			tkMessageBox.showerror( title = "Error", message = sys.exc_info( )[ 1 ] )

	def on_btn_click( self ):
		root = self.root_directory = tkFileDialog.askdirectory( parent = self, #initialdir = "/",
		                                                        title = "Please select a directory" )
		if root != "":
			for item in self.tv.get_children( ):
				self.tv.delete( item )
			self.btn.config(text="Working ...", state = DISABLED)
			self.master.config(cursor = "watch")
			self.tv.insert( "", END, iid = "root", text = "", values = (root, ""), open = True )
			t = threading.Thread(target=self.populate_items, args=(root,))
			t.daemon = 1
			t.start()

	def populate_items( self, root ):
		directories = [ d for d in os.listdir( root ) if os.path.isdir( os.path.join( root, d ) ) ]
		filesizes = [ ]
		for dirs in directories:
			filesize = 0
			items = [ ]
			for r, d, f in os.walk( os.path.join( root, dirs ) ):
				try:
					filesize += sum (os.path.getsize (os.path.join (r, name)) for name in f)
				except:
					pass
			filesizes.append( filesize )
			items.append( dirs )
			items.append( "{:.1f} MB".format( filesize / MB ) )
			self.queue.put( items, True )
		self.queue.put(['root', "{:.1f} MB".format(sum(filesizes) / MB)])
		self.queue.put( None )

	def treeview_sort_column( self, tv, col, reverse ):
		l = [ (tv.set( k, col ), k) for k in tv.get_children( "root" ) ]
		if col == "Size":
			l.sort( key = lambda tup: map( int, str( tup[ 0 ] )[ :-2 ].split( "." ) ), reverse = reverse )
		else:
			l.sort( reverse = reverse )

		for index, (val, k) in enumerate( l ):
			tv.move( k, "root", index )

		tv.heading( col, command = lambda: self.treeview_sort_column( tv, col, not reverse ) )

	def on_treeview_open( self, event ):
		pass

	def format_size( self, filesize ):
		if filesize >= GB:
			return "{:,.1f}GB".format( filesize / GB )
		if filesize >= MB:
			return "{:,.1f}MB".format( filesize / MB )
		if filesize >= kB:
			return "{:,.1f}kB".format( filesize / kB )
		return "{}bytes".format( filesize )


if __name__ == '__main__':
	root = Tk( )
	frame = RootFrame( root )
	frame.pack(fill="both", expand=True)
	root.title( title )
	root.mainloop( )
